describe('Test d\'une addition', () => {
  it('Je test ici de faire 2+3', () => {
    const a = 2;
    const b = 3;
    expect(a + b ).toEqual(6);
  });

  it('Je test ici de faire 3+3', () => {
    const a = 3;
    const b = 3;
    expect(a + b ).toEqual(6);
  });
});
