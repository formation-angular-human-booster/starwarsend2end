import { Component, OnInit } from '@angular/core';
import {Planet} from '../../../model/planet';
import {PlanetService} from '../../../services/planet.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {
  isLoading: boolean;
  planets: Planet[];
  constructor(private planetService: PlanetService) { }

  ngOnInit() {
    this.isLoading = true;
    this.planetService.getAllPlanets()
      .subscribe(planettes => {
      this.planets = planettes;
      this.isLoading = false;
    });
  }
  deletePlanete(planet: Planet) {
    this.isLoading = true;
    this.planetService.removePlanet(planet).subscribe(then => {
      this.planetService.getAllPlanets()
        .subscribe(planettes => {
          this.planets = planettes;
          this.isLoading = false;
        });
    });
  }
}
