import { Component, OnInit } from '@angular/core';
import {Planet} from '../../../model/planet';
import {PlanetService} from '../../../services/planet.service';
import {ActivatedRoute, Router} from '@angular/router';

// Composant qui gére la modification d'une planète
@Component({
  selector: 'app-edit-planet',
  templateUrl: './edit-planet.component.html',
  styleUrls: ['./edit-planet.component.css']
})
export class EditPlanetComponent implements OnInit {
  // Meme chose que pour la création il servira à afficher les options du select
  yesNoChoice = [{ text: 'oui', value: true}, { text: 'non', value: false}];
  // Objet planète à éditer
  planetForm: Planet;
  // Nous utiliserons notre planète service, le service
  // ActivatedRoute (pour recupérer l'url actuelle et l'id
  // qu'on lui passe en paramètre et le service router pour rediriger l'utilisateur quand
  // il a fini de traiter le formulaire
  constructor(private planetService: PlanetService, private activatedRoute: ActivatedRoute,
              private router: Router) { }
  // Nous recupérons l'id dans l'url grace au service ActivatedRoute puis nous reccupérons la planète à éditer
  // grace au service PlanetteService. Cette planete sera affecter à la variable PlaneteForm
  ngOnInit() {
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 0);
    this.planetService.getOnePlanet(id).subscribe(then => {
      this.planetForm = then;
    });
  }

  // Fonction qui est appelé lorsque notre formulaire est soumis.
  // Une fois soumis, on redirige l'utilisateur vers la liste de planète
  onSubmit() {
    // Nous appelons notre planete service pour mettre à jour notre planete
    this.planetService.editPlanet(this.planetForm);
    // Nous redirigeons l'utilisateur vers la liste de planete.
    this.router.navigate(['/planets']);
  }

}
