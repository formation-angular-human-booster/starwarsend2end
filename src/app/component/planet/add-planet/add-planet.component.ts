import { Component, OnInit } from '@angular/core';
import {Planet} from '../../../model/planet';
import {PlanetService} from '../../../services/planet.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-planet',
  templateUrl: './add-planet.component.html',
  styleUrls: ['./add-planet.component.css']
})
export class AddPlanetComponent implements OnInit {
  yesNoChoice = [{ text: 'oui', value: true}, { text: 'non', value: false}];
  planetForm: Planet;
  title = 'Page d\'ajout de planete';
  constructor(private planetService: PlanetService, private router: Router) { }
  ngOnInit() {
    this.planetForm = new Planet();
  }
  onSubmit(): void {
    this.planetService.addPlanet(this.planetForm).subscribe(after => {
      this.router.navigate(['/planets']);
    });

  }

}
