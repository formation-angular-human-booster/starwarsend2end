import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Planet} from '../../../model/planet';
import {PlanetService} from '../../../services/planet.service';

@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.css']
})
export class PlanetDetailComponent implements OnInit {
  planet: Planet;
  constructor(private activatedRoute: ActivatedRoute,
              private planetService: PlanetService) { }

  ngOnInit() {
    const id = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 0);
    this.planetService.getOnePlanet(id).subscribe(then => {
      this.planet = then;
    });

  }
}
