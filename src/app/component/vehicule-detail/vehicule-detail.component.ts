import { Component, OnInit } from '@angular/core';
import {Vehicule} from '../../model/vehicule';
import {VehiculeService} from '../../services/vehicule.service';
import {ActivatedRoute} from '@angular/router';
// Composant qui servira à afficher le détail d'un véhicule
@Component({
  selector: 'app-vehicule-detail',
  templateUrl: './vehicule-detail.component.html',
  styleUrls: ['./vehicule-detail.component.css']
})
export class VehiculeDetailComponent implements OnInit {
  // Variable qui contiendra le véhicule à afficher
  vehicule: Vehicule;
  // Injection de notre véhicule service dans le composant ainsi que du service angular
  // Activated route afin de récupérer l'id dans notre URL
  constructor(private vehiculeService: VehiculeService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    // Déclarer une variable qui sera égale à l'id passé dans
    // l'URL (cf : app.routing.module.ts)
    const id: number = parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 0);
    this.vehiculeService.getVehiculeById(id).subscribe(data => {
      this.vehicule = data;
    });
  }

}
