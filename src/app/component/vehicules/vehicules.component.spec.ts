import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiculesComponent } from './vehicules.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

describe('VehiculesComponent', () => {
  let component: VehiculesComponent;
  let fixture: ComponentFixture<VehiculesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiculesComponent ],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        FontAwesomeModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiculesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
