import { Component, OnInit } from '@angular/core';
import {VehiculeService} from '../../services/vehicule.service';
import {Vehicule} from '../../model/vehicule';
import {faHome} from '@fortawesome/free-solid-svg-icons';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';
// Composante VehiculesComponent qui permettra
// l'affichage de tous mes véhicules
@Component({
  selector: 'app-vehicules',
  templateUrl: './vehicules.component.html',
  styleUrls: ['./vehicules.component.css']
})
export class VehiculesComponent implements OnInit {
  // Je déclare la variable vehicules qui sera un tableau de véhicule
  vehicules: Vehicule[];
  // Ce conposant utilisera le service Vehicule service
  isLoading: boolean;
  faSpin = faSpinner;
  constructor(private vehiculeService: VehiculeService) { }

  ngOnInit() {
    // J'affecte à ma variable véhicule la fonction
    // getAllVehicules de mon service
    // Subscribe attend la réponse de la fonction
    // getAllVehicule du service puis execute la fonction passé en parametre
    // puis mettre la réponse dans la variable data
    this.isLoading = true;
    this.vehiculeService.getAllVehicules().subscribe(data => {
      this.vehicules = data;
      this.isLoading = false;
    });
  }

}
