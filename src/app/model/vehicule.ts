// Classe qui sert de moule pour pouvoir créer de nouveaux objets !
export class Vehicule {
  id: number;
  nom: string;
  carburant: boolean;
  image: string;
}
