export class Planet {
  id: number;
  nom: string;
  hasOxygen: boolean;
  image: string;
}
