import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './component/dashboard/dashboard.component';
import {VehiculesComponent} from './component/vehicules/vehicules.component';
import {PlanetsComponent} from './component/planet/planets/planets.component';
import {PlanetDetailComponent} from './component/planet/planet-detail/planet-detail.component';
import {VehiculeDetailComponent} from './component/vehicule-detail/vehicule-detail.component';
import {AddPlanetComponent} from './component/planet/add-planet/add-planet.component';
import {EditPlanetComponent} from './component/planet/edit-planet/edit-planet.component';


const routes: Routes =  [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home',      component: DashboardComponent },
  { path: 'vehicules',      component: VehiculesComponent },
  { path: 'vehicules/:id',      component: VehiculeDetailComponent },
  { path: 'planets',      component: PlanetsComponent },
  { path: 'planets/add-planet',      component: AddPlanetComponent, pathMatch: 'full' },
  { path: 'planets/:id',      component: PlanetDetailComponent },
  { path: 'planets/edit/:id',      component: EditPlanetComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
