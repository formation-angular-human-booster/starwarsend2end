import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './component/menu/menu.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { PlanetsComponent } from './component/planet/planets/planets.component';
import { VehiculesComponent } from './component/vehicules/vehicules.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PlanetDetailComponent } from './component/planet/planet-detail/planet-detail.component';
import { VehiculeDetailComponent } from './component/vehicule-detail/vehicule-detail.component';
import { AddPlanetComponent } from './component/planet/add-planet/add-planet.component';
import {FormsModule} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditPlanetComponent } from './component/planet/edit-planet/edit-planet.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    PlanetsComponent,
    VehiculesComponent,
    PlanetDetailComponent,
    VehiculeDetailComponent,
    AddPlanetComponent,
    EditPlanetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
