import {Planet} from '../model/planet';

export const PLANETS: Planet[] = [
  { id: 1, hasOxygen: true, nom: 'Aaragonar', image: 'https://vignette.wikia.nocookie.net/swfanon/images/0/08/KaleePlanet.jpg/revision/latest/scale-to-width-down/340?cb=20080706032606' },
  { id: 2, hasOxygen: false, nom: 'Lune', image: 'https://fr.ubergizmo.com/wp-content/uploads/2018/07/plus-belles-photos-astronomie-2018-6-1078x800.jpg' }
];
