import {Vehicule} from '../model/vehicule';

// Ne sachant pas encore utiliser une BDD j'utilise un
// fichier qui contient mes objets
export const VEHICULES: Vehicule[] = [
  {
    id: 1,
    nom: 'Star Destroyer',
    carburant: true,
    image: 'https://cdn.radiofrance.fr/s3/cruiser-production/2019/09/594bcd80-fe1b-483d-9348-959a9c8b7ac8/801x410_lego_destroyer.jpg',
  },
  {
    id: 2,
    nom: 'BTwin',
    carburant: false,
    image: 'https://www.topgear-magazine.fr/wp-content/uploads/2019/11/high_porsche_x_star_wars_sketch_2019_porsche_ag_4.jpg',
  }
];
