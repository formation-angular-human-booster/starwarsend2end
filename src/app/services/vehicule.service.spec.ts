import { TestBed } from '@angular/core/testing';

import { VehiculeService } from './vehicule.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('VehiculeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
  }));

  it('should be created', () => {
    const service: VehiculeService = TestBed.get(VehiculeService);
    expect(service).toBeTruthy();
  });
});
