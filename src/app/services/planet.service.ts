import { Injectable } from '@angular/core';
import {Planet} from '../model/planet';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {
  planets: Planet[];
  apiURL = 'http://localhost:3000/planet';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }

  getAllPlanets(): Observable<Planet[]> {
    return this.http.get<Planet[]>(this.apiURL)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  getOnePlanet(id: number): Observable<Planet> {
    return this.http.get<Planet>(this.apiURL + '/' + id )
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  addPlanet(planet: Planet): Observable<Planet> {
    return this.http.post<Planet>(this.apiURL, planet, this.httpOptions);
  }
  removePlanet(planet: Planet): Observable<Planet> {
    return this.http.delete<Planet>(this.apiURL + '/' + planet.id, this.httpOptions);
  }
  // Fonction edit retrouve la planète passé en paramètre dans notre tableau de
  // planètes et la met à jour en fonction de la planete qu'on lui donne.
  editPlanet(planet: Planet): void {
    this.planets
      .filter(planette => planette.id === planet.id)[0] = planet;
  }

  handleError(error) {
    let errorMessage = '';
    if ( error.error instanceof ErrorEvent ) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
