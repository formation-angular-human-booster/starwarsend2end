import { Injectable } from '@angular/core';
import {Vehicule} from '../model/vehicule';
import {VEHICULES} from '../data/mock-vehicule';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/internal/operators';
// Servive qui permettra de centraliser la gestion des données de nos véhicules
@Injectable({
  providedIn: 'root'
})
export class VehiculeService {
  // Url vers laquelle on tente de joindre les services
  apiUrl = 'http://localhost:3000/vehicule';
  // Elements additionels envoyés par la requête
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  // Fonction qui est appelé à la création du service,
  // nous injectons la dépendence vers HttpClient pour
  // pouvoir consommer nos web services
  constructor(private httpClient: HttpClient) { }
  // Fonction qui retourne tous nos véhicules
  getAllVehicules(): Observable<Vehicule[]> {
    // J'envoie une requete vers l'url des web services
    // J'attend une réponse si elle n'arrive pas ou n'est pas bonne ,
    // Je réessaye puis passe dans la fonction handleError
    return this.httpClient.get<Vehicule[]>(this.apiUrl, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  // Fonction qui retourne un objet vehicule depuis son id.
  // Elle prend en paramètre l'id de l'objet à retrouver
  getVehiculeById(id: number): Observable<Vehicule> {
    return this.httpClient.get<Vehicule>(this.apiUrl + '/' + id, this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  handleError(error) {
    return throwError(error);
  }
}
