import {browser, element, logging, by} from 'protractor';
import {PlanetPage} from './planet.po';

describe('test des planetes', () => {
  let page: PlanetPage;
  let nbPlanete: number;

  beforeEach(() => {
    page = new PlanetPage();
    browser.get('/planets');
  });

  it('Recherche le lien d\'ajout de planète et clique dessus', () => {
    element.all(by.css('li')).then(totalRows => {
      this.nbPlanete = totalRows.length;
      element(by.css('#addPlanetLink')).click();
      page.sleep();
      expect(browser.driver.getCurrentUrl()).toContain('planets/add-planet');
    });
  });

  it('Test le formulaire', () => {
    browser.get('/planets/add-planet');
    page.completeForm();
    page.sleep();
    let submitBtn = element.all(by.css('#submitBtn'));
    submitBtn.click().then(fn => {
      element.all(by.css('li')).then(totalNewRows => {
        this.nbPlanete += 1;
        const comparePlanet = this.nbPlanete;
        expect(totalNewRows.length).toEqual(comparePlanet);
        page.sleep();
      });
    });
  });

  it('Test de la suppression', () => {
    browser.get('/planets');
    page.sleep();
    let firstDeleteButton = element.all(by.css('.deletePlanet')).first();
    firstDeleteButton.click().then(fn => {
      element.all(by.css('li')).then(totalNewRows => {
        this.nbPlanete -= 1;
        const compare = this.nbPlanete ;
        expect(totalNewRows.length).toEqual(compare);
      });
    });
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
