import { browser, by, element } from 'protractor';

export class PlanetPage {

  sleep() {
    browser.driver.sleep(2500);
  }
  completeForm() {
    let nom = element.all(by.id('nom'));
    let hasOxygen = element.all(by.id('hasOxygen'));
    let image = element.all(by.id('image'));
    nom.sendKeys('test');
    hasOxygen.sendKeys('Oui');
    image.sendKeys('test');
  }
}
